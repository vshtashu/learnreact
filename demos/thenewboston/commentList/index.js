/**
 * Created by ashu on 26/7/16.
 */


var Comment = React.createClass({

	getInitialState: function () {
		return {editable: true};
	},

	edit: function () {
		this.setState({editable: true});
	},

	save: function () {
		this.setState({editable: false});
		this.props.updateComment(this.refs.newText.value, this.props.index);
	},

	remove: function () {
		this.props.removeComment(this.props.index);
	},

	cancel: function () {
		this.setState({editable: false});
	},

	renderForm: function () {
		return (
			<div>
				<textarea ref="newText" defaultValue={this.props.children}></textarea>
				<button onClick={this.save}>Save</button>
				<button onClick={this.cancel}>Cancel</button>
			</div>
		);
	},

	renderDisplay: function () {
		return (
			<div>
				<div> {this.props.children}</div>
				<button onClick={this.edit}>Edit</button>
				<button onClick={this.remove}>Remove</button>
			</div>
		);
	},

	render: function () {
		if (this.state.editable) {
			return this.renderForm();
		} else {
			return this.renderDisplay();
		}
	}

});

var CommentBoard = React.createClass({
	getInitialState: function () {
		return {
			comments: []
		}
	},

	addComment: function (text) {
		var comments = this.state.comments;
		comments.push(text);
		this.setState({comments: comments});
	},

	removeComment: function (i) {
		var comments = this.state.comments;
		comments.splice(i, 1);
		this.setState({comments: comments});
	},

	updateComment: function (newText, i) {
		var comments = this.state.comments;
		comments[i] = newText;
		this.setState({comments: comments});
	},

	eachComment: function (text, i) {
		return (<Comment key={i} index={i} addComment={this.addComment} removeComment={this.removeComment}
		                 updateComment={this.updateComment}>{text}</Comment>);
	},

	render: function () {
		return (
			<div>
				<div>
					<button onClick={this.addComment.bind(null,"... write here")}>+</button>
				</div>
				<div>
					{ this.state.comments.map(this.eachComment) }
				</div>


			</div>

		);
	}
});

ReactDOM.render(<CommentBoard />, document.getElementById("commentView"));