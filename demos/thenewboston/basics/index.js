/**
 * Created by ashu on 26/7/16.
 */

var TextTitle = React.createClass({
	render: function () {
		return (<h2>Hey! {this.props.children} here. </h2 >);
	}
});

var TextBody = React.createClass({
	render: function () {
		return (<p>Welcome to my page. Hope you have a {this.props.feeling} time. </p>);
	}
});

var TextPost = React.createClass({
	render: function () {
		return (
			<div>
				<TextTitle>Ashu</TextTitle>
				<TextBody feeling="nice"/>
				<TextTitle>Harshith</TextTitle>
				<TextBody feeling="good"/>
			</div>
		);
	}
});

ReactDOM.render(< TextPost />, document.getElementById("dem"));